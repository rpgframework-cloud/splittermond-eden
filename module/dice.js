import SplittermondRoll from "../module/dice/splittermond_roll.js"

export async function doRoll(data, messageData = {}) {

  messageData.flavor = data.title;  
  messageData.speaker = ChatMessage.getSpeaker();
  
  // Define the inner roll function
  const _roll = (type, form, data) => {

    let nd = 0;
    //Art des Wurfs
    // Risko
    if (type === -1) {
      messageData.flavor += `(Risikowurf)`;
      nd = 4;
    } else if (type === 0) {
      //normal
      nd = 2;
    } else if (type === 1) {
      //Sicherheitswurf
      messageData.flavor += `(Sicherheitswurf)`;
      nd = 2;
    }

    if (form) {
      data.mod = parseInt(form.mod.value);
      data.difficulty = parseInt(form.difficulty.value);
      data.type = type;
      messageData.rollMode = form.rollMode.value;
      data.formula = nd + "d10 + " + data.skill.value + " + " +   data.mod;
    }

    // Execute the roll
    let r = new SplittermondRoll("", data);
    try {
      r.evaluate();
    } catch (err) {
      console.error(err);
      ui.notifications.error(`Dice roll evaluation failed: ${err.message}`);
      return null;
    }
    return r;
  }

  // Create the Roll instance
  const _r = await _rollDialog({ data, foo: _roll });
  _r.toMessage(messageData);
  return _r;
}


/**
 * @return {Promise<Roll>}
 * @private
 */
async function _rollDialog({ data, foo } = {}) {

  if (isNaN(data.difficulty)) {
    data.difficulty = 15;
  }
  // Render modal dialog
  let template = "systems/splittermond/templates/chat/roll-dialog.html";
  let dialogData = {
    data: data,
    rollModes: CONFIG.Dice.rollModes,
  };
  const html = await renderTemplate(template, dialogData);
  const title = data.title;


  // Create the Dialog window
  return new Promise(resolve => {
    new Dialog({
      title: title,
      content: html,
      buttons: {
        risk: {
          label: "Risikowurf",
          callback: html => resolve(foo(-1, html[0].querySelector("form"), data))
        },
        normal: {
          label: "Normal",
          callback: html => resolve(foo(0, html[0].querySelector("form"), data))
        },
        security: {
          label: "Sicherheitswurf",
          callback: html => resolve(foo(1, html[0].querySelector("form"), data))
        }
      },
      default: "normal",
      close: () => resolve(null)
    }).render(true);
  });
}


