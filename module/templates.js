/**
 * Define a set of template paths to pre-load
 * Pre-loaded templates are compiled and cached for fast access when rendering
 * @return {Promise}
 */
export const preloadHandlebarsTemplates = async function() {

  const templatePaths = [
    "systems/splittermond/templates/parts/sheet-attributes.html",
    "systems/splittermond/templates/parts/pc-biography.html",
    "systems/splittermond/templates/parts/pc-skills.html",
    "systems/splittermond/templates/parts/pc-fightskills.html",
    "systems/splittermond/templates/parts/pc-magicskills.html",
    "systems/splittermond/templates/parts/pc-items.html",
  ];
  
  

  return loadTemplates(templatePaths);
};