
export default class SplittermondRoll extends Roll {

  static CHAT_TEMPLATE = "systems/splittermond/templates/chat/roll-splittermond.html";
  static TOOLTIP_TEMPLATE = "systems/splittermond/templates/chat/tooltip.html";

  constructor(...args) {
    super(...args);
    this.data = args[1];
    this.p = {};
  }

  /** @override */
  evaluate() {
    let total;
    let dieResult;
    let triumph = false;
    let patzer = false;
    let erfolgsgrade;
    let data = this.data;

    let results = [];
    //RISIKOWURF
    if (data.type == -1) {
      let die = new Die({ faces: 10, number: 4 }).evaluate();
      results = this.getSplittermondRolls(die);
      results.sort((a, b) => b.diceResult - a.diceResult);

      let highest = results[0].diceResult + results[1].diceResult;
      triumph = highest >= 19;
      let lowest = results[2].diceResult + results[3].diceResult;
      patzer = lowest <= 3;

      if (!patzer) {
        dieResult = highest;
        total = dieResult + data.mod + data.value;
        erfolgsgrade = Math.floor((((total - data.difficulty) / 3)));
        if (triumph) {
          erfolgsgrade += 3;
        }
        results[0].active = true;
        results[1].active = true;
      } else {
        triumph = false;
        dieResult = lowest;
        total = dieResult + data.mod + data.value;
        erfolgsgrade = Math.floor((((total - data.difficulty) / 3)));
        erfolgsgrade -= 3;
        erfolgsgrade = Math.min(erfolgsgrade, -1);
        results[2].active = true;
        results[3].active = true;
      }
      data.formula = "4W10";

    } else if (data.type == 0) {
      //NORMALER WURF
      let die = new Die({ faces: 10, number: 2 }).evaluate();
      results = this.getSplittermondRolls(die);
      let sum = results[0].diceResult + results[1].diceResult;
      triumph = sum >= 19;
      patzer = sum <= 3;
      results[0].active = true;
      results[1].active = true;

      dieResult = sum;
      total = dieResult + data.mod + data.value;
      if (!patzer) {
        erfolgsgrade = Math.floor((((total - data.difficulty) / 3)));
        if (triumph) {
          erfolgsgrade += 3;
        }
      } else {
        triumph = false;
        erfolgsgrade = Math.floor((((total - data.difficulty) / 3)));
        erfolgsgrade -= 3;
        erfolgsgrade = Math.min(erfolgsgrade, -1);
      }
      data.formula = "2W10";

    } else if (data.type == 1) {
      let die = new Die({ faces: 10, number: 2 }).evaluate();
      results = this.getSplittermondRolls(die);
      results.sort((a, b) => b.diceResult - a.diceResult);
      let sum = results[0].diceResult;
      dieResult = sum;
      total = dieResult + data.mod + data.value;
      erfolgsgrade = Math.floor((((total - data.difficulty) / 3)));
      results[0].active = true;
      results[1].active = false;
      triumph = false;
      patzer = false;
      data.formula = "2W10";
    }
    results.sort((a, b) => b.index - a.index);
    data.rolls = results;

    data.formula += " + " + data.value + " + " + data.mod + " (Schwierigkeit: " + data.difficulty + ")";
    data.dieResult = dieResult;
    data.total = total;
    data.erfolgsgrade = erfolgsgrade;
    data.triumph = triumph;
    data.patzer = patzer;

    data.success = total >= data.difficulty && !patzer;
    data.egText = this.getEGText(erfolgsgrade, data.success);

    this._rolled = true;
    this._total = total;
    this._formula = data.formula;
    return this;
  }


  getSplittermondRolls(die) {
    let index = 0;
    let results = []
    die.results.forEach(r => {
      let x = {};
      x.diceResult = r.result;
      x.index = index;
      x.active = false;
      index++;
      results.push(x);
    });
    return results;
  }

  getEGText(erfolgsgrade, success) {
    if (erfolgsgrade >= 5) {
      return "Herausragend gelungen";
    } else if (erfolgsgrade >= 3) {
      return "Gut gelungen";
    } else if (erfolgsgrade >= 1) {
      return "Gelungen";
    } else if (erfolgsgrade == 0 && success) {
      return "Knapp gelungen";
    } else if (erfolgsgrade == 0) {
      return "Knapp misslungen";
    } else if (erfolgsgrade >= -2) {
      return "Misslungen";
    } else if (erfolgsgrade >= -4) {
      return "Schwer misslungen";
    } else {
      return "Verheerend misslungen";
    }
  }
  /* -------------------------------------------- */
  /** @override */
  roll() {
    return this.evaluate();
  }

  /* -------------------------------------------- */
  /** @override */
  getTooltip() {
    let parts = {};
    return renderTemplate(this.constructor.TOOLTIP_TEMPLATE, { parts, data: this.data });
  }

  /* -------------------------------------------- 
  * Hier wird die Ausgabe zusammengeschustert
  */
  async render(chatOptions = {}) {
    chatOptions = mergeObject(
      {
        user: game.user._id,
        flavor: this.flavorText,
        template: this.constructor.CHAT_TEMPLATE,
      },
      chatOptions
    );


    let isPrivate = chatOptions.isPrivate;

    const chatData = {
      formula: isPrivate ? "???" : this._formula,
      flavor: isPrivate ? null : chatOptions.flavor,
      user: chatOptions.user,
      tooltip: isPrivate ? "" : await this.getTooltip(),
      total: isPrivate ? "?" : Math.round(this.total * 100) / 100,
      erfolgsgrade: isPrivate ? "?" : this.data.erfolgsgrade,
      data: this.data,
      publicRoll: !chatOptions.isPrivate,
    };

    let html = await renderTemplate(chatOptions.template, chatData);
    return html;
  }

  /* -------------------------------------------- */
  async toMessage(chatOptions, { rollMode = null, create = true } = {}) {

    const rMode = rollMode || chatOptions.rollMode || game.settings.get("core", "rollMode");

    let template = CONST.CHAT_MESSAGE_TYPES.OTHER;
    if (["gmroll", "blindroll"].includes(rMode)) {
      chatOptions.whisper = ChatMessage.getWhisperRecipients("GM");
    }
    if (rMode === "blindroll") chatOptions.blind = true;
    if (rMode === "selfroll") chatOptions.whisper = [game.user.id];

    // Prepare chat data
    chatOptions = mergeObject(
      {
        user: game.user._id,
        type: CONST.CHAT_MESSAGE_TYPES.ROLL,
        content: this.total,
        sound: CONFIG.sounds.dice,
      },
      chatOptions
    );
    chatOptions.roll = this;
    chatOptions.content = await this.render(chatOptions);
    ChatMessage.create(chatOptions);
  }

    /** @override */
    toJSON() {
      const json = super.toJSON();
      json.erfolgsgrade = this.data.erfolgsgrade;
      json.data = this.data;
      return json;
    }
  
    /** @override */
    static fromData(data) {
      const roll = super.fromData(data);
      roll.data = data.data;
      roll.erfolgsgrade = data.data.erfolgsgrade;
      return roll;
    }
}

