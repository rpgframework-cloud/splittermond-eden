import { Helper } from '../helper.js';

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class SplittermondPCSheet extends ActorSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["splittermond", "sheet", "actor", "pc"],
      template: "systems/splittermond/templates/splittermond-pc-sheet.html",
      width: 700,
      height: 900,
      scrollY: [".inventory", ".attributes", ".biography", ".skills", ".magic", ".fight"],
      tabs: [{ navSelector: ".tabs", contentSelector: ".sheet-body", initial: "skills" }],
      //dragDrop: [{ dragSelector: ".item-list .item", dropSelector: null }]
    });
  }

  /** @overrride */
  getData() {
    let data = super.getData();
    data.config = CONFIG.SPLIMO;
    return data;
  }

  /* -------------------------------------------- */
  /*  Event Listeners and Handlers
  /* -------------------------------------------- */

  /**
   * Activate event listeners using the prepared sheet HTML
   * @param html {HTML}   The prepared HTML object ready to be rendered into the DOM
   */
  activateListeners(html) {
    // Owner Only Listeners
    if (this.actor.owner) {
      // Roll Skill Checks
      html.find('.skill-name').click(this._onRollSkillCheck.bind(this));
      html.find('.weapon-skillname').click(this._onRollWeaponSkillCheck.bind(this));
      html.find('.spell-skillname').click(this._onRollSpellSkillCheck.bind(this));
      html.find('#btnShortRest').click(this._onShortRest.bind(this));
      html.find('#btnLongRest').click(this._onLongRest.bind(this));
      html.find("#dataFOmax").on("input", this._onRecalculateFO(html));
      html.find(".calcLE").on("input", this._onRecalculateLE(html));
      html.find('.weapon-speed').change(this._onChangeWeaponSpeed.bind(this));
      html.find('.weapon-range').change(this._onChangeWeaponRange.bind(this));
      html.find('.weapon-mod').change(this._onChangeWeaponMod.bind(this));
      html.find('.weapon-damage').change(this._onChangeWeaponDamage.bind(this));
      html.find('.primary-weapon').change(this._onSetPrimaryWeapon.bind(this));
      html.find('.item-delete').click(this._onItemDelete.bind(this));
      html.find(".splimo-collapsible").click(this._onCollapsibleClick.bind(this));
      html.find(".skill-filter").click(this._onFilterSkills.bind(this));
      html.find(".skill-filter").change(this._onChangeInput.bind(this));

    html.find('.spell-create').click(ev => {
      const itemData = {
        name: "Neuer Zauber" ,
        type: "spell",
      };
      return this.actor.createEmbeddedEntity("OwnedItem", itemData);
    });

    html.find('.item-edit').click(ev => {
      const element = ev.currentTarget.closest(".item");
      const item = this.actor.getOwnedItem(element.dataset.itemId);
      item.sheet.render(true);
    });

    
    }
    else {
      html.find(".rollable").each((i, el) => el.classList.remove("rollable"));
    }

    // Handle default listeners last so system listeners are triggered first
    super.activateListeners(html);
  }

  _onChangeInput(event) {
    if (event.currentTarget.classList.contains("skill-filter")) {
      event.preventDefault();
    } else {
      super._onChangeInput(event);
    }
  }
  //-----------------------------------------------------
  /**
   * Handle rolling a Skill check
   * @param {Event} event   The originating click event
   * @private
   */
  _onRollSkillCheck(event, html) {
    event.preventDefault();
    const skill = event.currentTarget.dataset.skill;
    this.actor.rollSkill(skill, { event: event });

  }


  _onRollWeaponSkillCheck(event) {
    event.preventDefault();
    const itemId = event.currentTarget.dataset.item;
    const itemType = event.currentTarget.dataset.itemType;
    this.actor.rollWeaponSkill(itemId, itemType, { event: event });
  }

  _onRollSpellSkillCheck(event) {
    event.preventDefault();
    const itemId = event.currentTarget.dataset.item;
    this.actor.rollSpellSkill(itemId, { event: event });
  }


  _onShortRest(event) {
    event.preventDefault();
    console.log("Before calling actor.shortRest  " + this.form);
    this.actor.shortRest();
    console.log("After calling actor.shortRest  " + this.actor);
    this.actor.render();
  }

  _onLongRest(event) {
    event.preventDefault();
    console.log("Before calling actor.longRest  " + this.form);
    this.actor.longRest();
    this.actor.render();
  }

  //-----------------------------------------------------
  _onRecalculateLE(html) {
    let vMax = parseInt(html.find("#dataLEmax")[0].value);
    let vVer = parseInt(html.find("#dataLEverz")[0].value);
    let vKan = parseInt(html.find("#dataLEkan")[0].value);
    let vErs = parseInt(html.find("#dataLEers")[0].value);
    let totalVer = vMax - vVer;  // Wieviel nach Verschnaufpause
    let percVerz = totalVer / vMax * 100;
    html.find("#barLEVerz")[0].style.width = percVerz + "%";
    let totalKan = totalVer - vErs; // Rest + Kanalisiert
    let percKan = totalKan / totalVer * 100;
    html.find("#barLEKan")[0].style.width = percKan + "%";
    let totalCur = vMax - vVer - vKan - vErs;
    let percCur = totalCur / totalKan * 100;
    html.find("#barLECur")[0].style.width = percCur + "%";
    html.find("#dataLEcur")[0].value = totalCur;

    this.object.data.data.attr2.lp.cur = totalCur;
  }

  //-----------------------------------------------------
  _onRecalculateFO(html) {
    let vMax = parseInt(html.find("#dataFOmax")[0].value);
    let vVer = parseInt(html.find("#dataFOverz")[0].value);
    let vKan = parseInt(html.find("#dataFOkan")[0].value);
    let vErs = parseInt(html.find("#dataFOers")[0].value);
    let totalVer = vMax - vVer;  // Wieviel nach Verschnaufpause
    let percVerz = totalVer / vMax * 100;
    html.find("#barFOVerz")[0].style.width = percVerz + "%";
    let totalKan = totalVer - vErs; // Rest + Kanalisiert
    let percKan = totalKan / totalVer * 100;
    html.find("#barFOKan")[0].style.width = percKan + "%";
    let totalCur = vMax - vVer - vKan - vErs;
    let percCur = totalCur / totalKan * 100;
    html.find("#barFOCur")[0].style.width = percCur + "%";
    html.find("#dataFOcur")[0].value = totalCur;
    this.object.data.data.attr2.fo.cur = totalCur;

    //    $.get(
    //    		"http://www.rpgframework.de", 
    //    		function(data){
    //    			console.log(data)
    //    		})
  }

  async _onChangeWeaponSpeed(event) {
    const iid = Helper.listItemId(event);
    const item = this.actor.getOwnedItem(iid);
    const speed = parseInt(event.currentTarget.value);
    if (item && (speed || speed == 0)) {
      item.update({ 'data.speed': speed });
    }
  }

  async _onChangeWeaponRange(event) {
    const iid = Helper.listItemId(event);
    const item = this.actor.getOwnedItem(iid);
    const range = parseInt(event.currentTarget.value);
    if (item && (range || range == 0)) {
      item.update({ 'data.range': range });
    }
  }

  async _onChangeWeaponDamage(event) {
    const iid = Helper.listItemId(event);
    const item = this.actor.getOwnedItem(iid);
    const damage = event.currentTarget.value;
    if (item && damage) {
      item.update({ 'data.damage': damage });
    }
  }

  async _onChangeWeaponMod(event) {
    const iid = Helper.listItemId(event);
    const item = this.actor.getOwnedItem(iid);
    const skillMod = parseInt(event.currentTarget.value);
    if (item && (skillMod) || skillMod == 0) {
      item.update({ 'data.skillMod': skillMod });
    }
  }

  async _onSetPrimaryWeapon(event) {
    event.preventDefault();
    let iid = Helper.listItemId(event);
    const item = this.actor.getOwnedItem(iid);
    if (item) {
      item.update({ 'data.primary': true });
      //alle anderen auf false setzen
      this.actor.items.entries.forEach(element => {
        if (element.type == "weapon" && element._id != iid) {
          element.update({ 'data.primary': false });
        }
      });
    }
  }

  /**
 * Handle deleting an existing Owned Item for the Actor
 * @param {Event} event   The originating click event
 * @private
 */
  _onItemDelete(event) {
    event.preventDefault();
    const li = event.currentTarget.closest(".item");
    this.actor.deleteOwnedItem(li.dataset.itemId);
  }

  _onCollapsibleClick(event) {
    event.preventDefault();
    event.currentTarget.classList.toggle("collapsible-active");
    var content = event.currentTarget.nextElementSibling;
    if (content.style.maxHeight) {
      content.style.maxHeight = null;
    } else {
      content.style.maxHeight = content.scrollHeight + "px";
    }
  }

  _onFilterSkills(event) {
    let table = this._getFilterableTable(event.currentTarget.parentNode);
    let checked = event.currentTarget.checked;
    var tr, input, i, inputValue, td;
   // table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");
    let filterCol = parseInt(table.attributes.filtercol.value);
    for (i = 0; i < tr.length; i++) {
      td = tr[i].getElementsByTagName("td")[filterCol];
      if (td) {
        input = td.getElementsByTagName("input");
        if (input) {
          inputValue = input[0].valueAsNumber;
          if (inputValue > 0 || !checked) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }
      }
    }
  }


  _getFilterableTable(node) {
    if (node.classList.contains("filterable")) {
      return node;
    } else if (node.parentNode) {
      return this._getFilterableTable(node.parentNode);
    } else {
      return null;
    }
  }
}