/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class SplittermondSpellSheet extends ItemSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["splittermond", "sheet", "item", "spell"],
      template: "systems/splittermond/templates/splittermond-spell-sheet.html",
      width: 400,
      height: 500,
    });
  }

}