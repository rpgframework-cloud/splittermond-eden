/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class SplittermondGearSheet extends ItemSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["splittermond", "sheet", "item", "gear"],
      template: "systems/splittermond/templates/splittermond-gear-sheet.html",
      width: 400,
      height: 500,
    });
  }

  getData() {
    let data = super.getData();
    data.config = CONFIG.SPLIMO;
    return data;
  }

}