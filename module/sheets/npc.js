/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class SplittermondNPCSheet extends ActorSheet {

    /** @override */
    static get defaultOptions() {
      return mergeObject(super.defaultOptions, {
        classes: ["splittermond", "sheet", "actor", "npc"],
        template: "systems/splittermond/templates/splittermond-npc-sheet.html",
        width: 600,
        height: 600,
        tabs: [{navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description"}],
        scrollY: [".biography", ".items", ".attributes"],
        dragDrop: [{dragSelector: ".item-list .item", dropSelector: null}]
      });
    }
  }