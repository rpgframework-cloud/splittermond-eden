import { doRoll } from "./dice.js";

/**
 * Extend the base Actor entity by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */
export class SplittermondActor extends Actor {
  /** @Override */
  prepareData() {
    super.prepareData();
    let actorData = this.data;
    this._calculateAttributeValues(actorData);
    this._calculateSkillValues(actorData);
    this._prepareItems(actorData);
    this._setPrimaryWeapon(actorData);
  }

  /**
 * Roll a Skill Check
 * Prompt the user for input regarding Advantage/Disadvantage and any Situational Bonus
 * @param {string} skillId      The skill id (e.g. "ins")
 * @param {Object} options      Options which configure how the skill check is rolled
 * @return {Promise<Roll>}      A Promise which resolves to the created Roll instance
 */
  rollSkill(skillId, options = {}) {
    const skl = this.data.data.skills[skillId];
    const value = skl.value
    const parts = [];


    // Roll and return
    let data = mergeObject(options, {
      parts: parts,
      value: value,
      title: skl.name,
      skill: skl
    });
    data.speaker = ChatMessage.getSpeaker({ actor: this });
    return doRoll(data);
  }

  /**
  * Roll a Skill Check
  * Prompt the user for input regarding Advantage/Disadvantage and any Situational Bonus
  * @param {string} skillId      The skill id (e.g. "ins")
  * @param {Object} options      Options which configure how the skill check is rolled
  * @return {Promise<Roll>}      A Promise which resolves to the created Roll instance
  */
  rollWeaponSkill(itemId, itemType, options = {}) {
    let item;
    if (itemType == "longrange") {
      item = this.data.data.longrangeweapons[itemId];
    } else {
      item = this.data.data.weapons[itemId];
    }
    if (!item) {
      this.data.items.forEach(element => {
        if (element._id == itemId) {
          item = element;
        }
      });
    }
    const skl = this.data.data.skills[item.data.skill];
    const value = item.data.skillValue;
    const parts = [];


    // Roll and return
    let data = mergeObject(options, {
      parts: parts,
      value: value,
      title: item.name + " (" + skl.name + ")",
      skill: skl,
      item: item,
      itemId: itemId
    });
    data.speaker = ChatMessage.getSpeaker({ actor: this });
    return doRoll(data);
  }

  rollSpellSkill(itemId, options = {}) {
    let item = this.data.data.spells[itemId];
    if (!item) {
      this.data.items.forEach(element => {
        if (element._id == itemId) {
          item = element;
        }
      });
    }
    const skl = this.data.data.skills[item.data.skill];
    const value = item.data.skillValue;
    const parts = [];
    let difficulty = item.data.diff;
    let extraText;
    if (item.data.diff == -1) {
      //defense
      difficulty = 0;
      extraText = "Der Zauber geht gegen die Verteidung, diese bitte als Schwierigkeit berücksichtigen."
    } else if (item.data.diff == -2) {
      //mindresist
      extraText = "Der Zauber geht gegen den geistigen Widerstand, diesen bitte als Schwierigkeit berücksichtigen."
      difficulty = 0;
    } else if (item.data.diff == -3) {
      //bodyresist
      extraText = "Der Zauber geht gegen den körperlichen Widerstand, diesen bitte als Schwierigkeit berücksichtigen."
      difficulty = 0;
    }

    // Roll and return
    let data = mergeObject(options, {
      parts: parts,
      value: value,
      title: item.name + " (" + skl.name + ")",
      skill: skl,
      difficulty: difficulty,
      item: item,
      itemId: itemId,
      extraText: extraText
    });
    data.speaker = ChatMessage.getSpeaker({ actor: this });
    return doRoll(data);
  }


  _calculateAttributeValues(actorData) {
    let data = actorData.data;
    for (let [key, attr] of Object.entries(data.attr)) {
      attr.current = parseInt(attr.value) + parseInt(attr.mod);
    }
  }

  _calculateSkillValues(actorData) {
    let data = actorData.data;
    for (let [key, skill] of Object.entries(data.skills)) {
      skill.value = 0;
      let attr1 = skill.attribute1;
      let attr2 = skill.attribute2;
      if (attr1 && attr1 != "") {
        skill.value = parseInt(data.attr[attr1.toLowerCase()].current);
      }
      if (attr2 && attr2 != "") {
        skill.value += parseInt(data.attr[attr2.toLowerCase()].current);
      }
      skill.value += parseInt(skill.modifier) + parseInt(skill.points);
    }
  }


  _setPrimaryWeapon(actorData) {
    for (let [key, item] of Object.entries(actorData.data.weapons)) {
      if (item.data.primary) {
        actorData.primaryWeaponId = item._id;
        actorData.primaryWeaponName = item.name;
        actorData.primaryWeaponSkillName = CONFIG.SPLIMO.fightskills[item.data.skill];
      }
    }
  }

  _prepareItems(actorData) {
    let data = actorData.data;
    data.weapons = [];
    data.longrangeweapons = [];
    data.normalItems = [];
    data.spells = []
    data.powers = []
    data.resources = []
    data.weaknesses = []
    for (let [key, item] of Object.entries(actorData.items)) {
      if (item.type == "weapon") {
        let skill = actorData.data.skills[item.data.skill];
        if (item.data.skill) {
          let attr1 = item.data.attribute1.toLowerCase();
          item.data.attribute1 = attr1;
          let attr2 = item.data.attribute2.toLowerCase();
          item.data.attribute2 = attr2;
          if (attr1) {
            item.data.skillValue = parseInt(data.attr[attr1].current);
          }
          if (attr2) {
            item.data.skillValue += parseInt(data.attr[attr2].current);
          }
          item.data.skillValue += parseInt(skill.modifier) + parseInt(skill.points) ;
          if (item.data.skillMod >= 0 || item.data.skillMod < 0) {
            item.data.skillValue +=  parseInt(item.data.skillMod);
          }
        }
        if (item.data.range > 0) {
          data.longrangeweapons.push(item);
        } else {
        data.weapons.push(item);
        }
      } else if (item.type == "normal") {
        data.normalItems.push(item);
      } else if (item.type == "spell") {
        if (item.data.skill) {
          let skill = actorData.data.skills[item.data.skill];
          item.data.skillValue = skill.value;
          if (item.data.skillMod >= 0 || item.data.skillMod < 0) {
            item.data.skillValue +=  parseInt(item.data.skillMod);
          }
          item.data.skillName = skill.name;
          item.data.diffText = item.data.diff;
          if (item.data.diff == -1) {
            //defense
           item.data.diffText = "VTD";
          } else if (item.data.diff == -2) {
            //mindresist
            item.data.diffText = "GW";
          } else if (item.data.diff == -3) {
            //bodyresist
            item.data.diffText = "KW";
          }
        }
        data.spells.push(item);
      } else if (item.type == "power") {
          data.powers.push(item);
      } else if (item.type == "resource") {
          data.resources.push(item);
      } else if (item.type == "weakness") {
          data.weaknesses.push(item);
      }
    }
  }



  /**
   * Erschöpfte Lebenspunkte zurücksetzen
   */
  shortRest(actorData) {
    this.data.data.attr2.lp.ers = 0;
    this.data.data.attr2.fo.ers = 0;
  }

  /**
   * Erschöpfte Lebenspunkte zurücksetzen
   */
  longRest(actorData) {
	  // Fokus
	  this.data.data.attr2.fo.ers = 0;
	  this.data.data.attr2.fo.kan = 0;
	  maxBack = this.data.data.attr.wil.current*2;
	  // ToDo: Erhöhte Fokusregeneration
      this.data.data.attr2.fo.verz = this.data.data.attr2.fo.verz -maxBack;
      if ( this.data.data.attr2.fo.verz<0)
    	 this.data.data.attr2.fo.verz=0;
      // LeP
      this.data.data.attr2.lp.ers = 0;
	  maxBack = this.data.data.attr.kon.current*2;
	  // ToDo: Erhöhte Lebensenergieregenration
      this.data.data.attr2.lp.verz =  this.data.data.attr2.lp.verz  - maxBack;
      if ( this.data.data.attr2.lp.verz<0)
    	 this.data.data.attr2.lp.verz=0;
  }
}
