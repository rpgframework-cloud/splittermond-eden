
// Namespace Configuration Values
export const SPLIMO = {};
/**
 * The set of skill which can be trained
 * @type {Object}
 */
SPLIMO.fightskills = {
    "melee": "Handgemenge",
    "slashing": "Hiebwaffen",
    "chains": "Kettenwaffen",
    "blades": "Klingenwaffen",
    "longrange": "Schusswaffen",
    "staffs": "Stangenwaffen",
    "throwing": "Wurfwaffen"
};
SPLIMO.normalskills = {
    "acrobatics": "Akrobatik",
    "alchemy": "Alchemie",
    "leadership": "Anführen",
    "arcanelore": "Arkane Kunde",
    "athletics": "Athletik",
    "performance": "Darbietung",
    "diplomacy": "Diplomatie",
    "clscraft": "Edelhandwerk",
    "empathy": "Empathie",
    "determination": "Entschlossenheit",
    "dexterity": "Fingerfertigkeit",
    "history": "Geschichte und Mythen",
    "craftmanship": "Handwerk",
    "heal": "Heilkunde",
    "stealth": "Heimlichkeit",
    "hunting": "Jagdkunst",
    "countrylore": "Länderkunde",
    "nature": "Naturkunde",
    "eloquence": "Redegewandtheit",
    "locksntraps": "Schlösser und Fallen",
    "swim": "Schwimmen",
    "seafaring": "Seefahrt",
    "streetlore": "Straßenkunde",
    "animals": "Tierführung",
    "survival": "Überleben",
    "perception": "Wahrnehmung",
    "endurance": "Zähigkeit"
};
SPLIMO.magicskills = {
    "antimagic": "Bannmagie",
    "controlmagic": "Beherrschungsmagie",
    "motionmagic": "Bewegungsmagie",
    "insightmagic": "Erkenntnismagie",
    "stonemagic": "Felsmagie",
    "firemagic": "Feuermagie",
    "healmagic": "Heilungsmagie",
    "illusionmagic": "Illusionsmagie",
    "combatmagic": "Kampfmagie",
    "lightmagic": "Lichtmagie",
    "naturemagic": "Naturmagie",
    "shadowmagic": "Schattenmagie",
    "fatemagic": "Schicksalsmagie",
    "protectionmagic": "Schutzmagie",
    "enhancemagic": "Stärkungsmagie",
    "deathmagic": "Todesmagie",
    "transformationmagic": "Verwandlungsmagie",
    "watermagic": "Wassermagie",
    "windmagic": "Windmagie"
};
SPLIMO.attributes = {
    "bew": "BEW",
    "aus": "AUS",
    "inn": "INT",
    "sta": "STÄ",
    "kon": "KON",
    "mys": "MYS",
    "wil": "WIL",
    "ver": "VER"
};
