import { SplittermondPCSheet } from "./sheets/pc.js";
import { SplittermondNPCSheet } from "./sheets/npc.js";
import { SplittermondSpellSheet } from "./sheets/spell.js";
import { SplittermondGearSheet } from "./sheets/gear.js";
import { SplittermondActor } from "./splittermondactor.js";
import { preloadHandlebarsTemplates } from "./templates.js";
import SplittermondRoll from "./dice/splittermond_roll.js";
import { SPLIMO } from "./config.js";

/* -------------------------------------------- */
/*  Foundry VTT Initialization                  */
/* -------------------------------------------- */

/**
 * Init hook.
 */
Hooks.once("init", async function () {

  console.log(`Initializing Splittermond System`);

   // Create a namespace within the game global
   game.splimo = {
    config: SPLIMO,
  };

  // Record Configuration Values
  CONFIG.SPLIMO = SPLIMO;

  // Define custom Entity classes
  CONFIG.Actor.entityClass = SplittermondActor;

   // Define custom Roll class
   CONFIG.Dice.rolls.push(SplittermondRoll);

  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("splittermond", SplittermondPCSheet, { types: ["pc"], makeDefault: true });
  Actors.registerSheet("splittermond", SplittermondNPCSheet, { types: ["npc"], makeDefault: true });
  Items.registerSheet("splittermond", SplittermondSpellSheet, { types: ["spell"], makeDefault: true });
  Items.registerSheet("splittermond", SplittermondGearSheet, { types: ["weapon", "normal"], makeDefault: true });
  console.log(Actors.registeredSheets);
  //   Items.unregisterSheet("core", ItemSheet);
  // Items.registerSheet("worldbuilding", SimpleItemSheet, { makeDefault: true });
  preloadHandlebarsTemplates();

  Handlebars.registerHelper('if_eq', function (a, b, block) {
    return a == b
      ? block(this)
      : block.inverse(this);
  });
  Handlebars.registerHelper( 'concat', function(op1,op2) {
	  return op1+op2;
	});


  // Allows {if X = Y} type syntax in html using handlebars
  Handlebars.registerHelper("iff", function (a, operator, b, opts) {
    var bool = false;
    switch (operator) {
      case "==":
        bool = a == b;
        break;
      case ">":
        bool = a > b;
        break;
      case "<":
        bool = a < b;
        break;
      case "!=":
        bool = a != b;
        break;
      case "contains":
        if (a && b) {
          bool = a.includes(b);
        } else {
          bool = false;
        }
        break;
      default:
        throw "Unknown operator " + operator;
    }

    if (bool) {
      return opts.fn(this);
    } else {
      return opts.inverse(this);
    }
  });
  
  Handlebars.registerHelper("math", function(lvalue, operator, rvalue, options) {
	    if (arguments.length < 4) {
	        // Operator omitted, assuming "+"
	        options = rvalue;
	        rvalue = operator;
	        operator = "+";
	    }
	        
	    lvalue = parseFloat(lvalue);
	    rvalue = parseFloat(rvalue);
	        
	    return {
	        "+": lvalue + rvalue,
	        "-": lvalue - rvalue,
	        "*": lvalue * rvalue,
	        "/": lvalue / rvalue,
	        "%": lvalue % rvalue
	    }[operator];
	});

  // Found in https://stackoverflow.com/questions/53398408/switch-case-with-default-in-handlebars-js
  Handlebars.registerHelper('switch', function(value, options) {
	  this.switch_value = value;
	  this.switch_break = false;
	  return options.fn(this);
	});

	Handlebars.registerHelper('case', function(value, options) {
	  if (value == this.switch_value) {
	    this.switch_break = true;
	    return options.fn(this);
	  }
	});

	Handlebars.registerHelper('default', function(value, options) {
	   if (this.switch_break == false) {
	     return value;
	   }
	});
});

