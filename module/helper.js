export class Helper {

    static listItemId(event) {
        return event.currentTarget.closest('.list-item').dataset.itemId;
    }

}